<?php
class Email_templating 
{
    protected $CI;
    protected $vars = array(
        'subject' => 'Test Subject',
        'lang_id' => '1',
        'from_email' => SITE_DEFAULT_SENDER_EMAIL,
        'from_name' => SITE_DEFAULT_SENDER_NAME,
        'files' => array(),
    );
    protected $record;
            
    function __construct(){
        $this->CI = & get_instance();
        //$this->CI->load->model('email_templates_m');
        //$this->CI->load->model('email_logs_m');
    }
    /**
     * config
     * Sets config for the the output that will be sending on the email.
     * 
     * @params string
     * @params array
    */
    function config($vars = array())
    {
        //set defaults
        $this->vars = array(
            'subject' => 'Test Subject',
            'lang_id' => 'english',
            'template' => '',
            'from_email' => SITE_DEFAULT_SENDER_EMAIL,
            'from_name' => SITE_DEFAULT_SENDER_NAME,
			'body'				=> '',
            'files' => array(),
            'others' => array(),
        );
        
        $this->vars = $this->set_params($this->vars, $vars);
        $vars = $this->vars;
        
     
    }
    /**
     * set_subject
     * 
     * Method sets subject overiding the default subject or the subject from pulled from the database.
     *  - This must be set after the config function and before the send function
     * 
     * @params string
     * @return none
     */    
    function set_subject($subject) {
        
       if ( empty($subject) ) {
           $subject = $this->vars['subject'];
       }
       
       $this->vars['subject'] = $subject;
    }
    /**
     * get_message
     * Method gets the generated message replacing the placeholder(s).
     *  
     * @return string
     */
    function get_message() {
		$this->vars['body'];
		/*
        $vars = $this->vars;
        
        if ( empty($vars['params'])) {
            return $this->record['body'];
        }
        
        return str_replace(array_keys($vars['params']), array_values($vars['params']), $this->record['body']);
		*/
    }
    /**
     * Method send generated message into an email.
     *  
     * @return bool
     */
    function send ($print_debugger =FALSE) {     
        $url = 'http://sendgrid.com/';
        $user = 'toby.roman';
        $pass = 'TobyRoman123'; 

        //$data['content'] = $this->get_message();
        $data['content'] = $this->vars['body'];
   
        
        $template = 'email_templates/standard_responsive';
        
        if (! empty($this->vars['template']))
        {
            $template = $this->vars['template'];
        }
        
        if (! empty($this->vars['others']))
        {
            $data['others'] = $this->vars['others'];
        }
        
        $body = $this->CI->load->view($template, $data, TRUE, false, false);
        
        $params = array(
            'api_user'  => $user,
            'api_key'   => $pass,
            'to'        => $this->vars['to'],
            'subject'   => $this->vars['subject'],
            'html'      => $body,
            'text'      => $body,
            'from'      => $this->vars['from_email'],
            'fromname'  => $this->vars['from_name'],
        );
        
        //Adding attachment
        if (! empty($this->vars['files'])) {
            foreach ($this->vars['files'] as $file) {
                $params['files['.$file['filename'].']'] = '@'.$file['path'];
            }
        }
        
        $request =  $url.'api/mail.send.json';

        // Generate curl request
        $session = curl_init($request);
        // Tell curl to use HTTP POST
        curl_setopt ($session, CURLOPT_POST, true);
        // Tell curl that this is the body of the POST
        curl_setopt ($session, CURLOPT_POSTFIELDS, $params);
        // Tell curl not to return headers, but do return the response
        curl_setopt($session, CURLOPT_HEADER, false);
        curl_setopt($session, CURLOPT_RETURNTRANSFER, true);

        // obtain response
        $response = curl_exec($session);
        curl_close($session);

        //print everything out
        $response = json_decode($response);
        //$print_debugger = TRUE;
        if ($print_debugger) {
            echo '<pre>';
            print_r($response);
            exit;
        }
        
        
        if (isset($response->message)) {
           
            if ($response->message != 'success') {
                return FALSE;
            }
            else {
                return TRUE;
            }
        }
        
      
        
        return FALSE;
    }
    /**
     * set_params
     * 
     * Set parameters values
     * 
     * @access public
     * @param array
     * @param array
     * @return array
     */
    function set_params($params, $args)
    {
            foreach ($args as $i => $value)
            {
                    if (isset($args[$i]))
                    {
                            $params[$i] = $value;
                    }
            }

            return $params;
    }
}
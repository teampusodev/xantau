<?php
$lang["email_already_exist_try_new_one"]="Email already exists. Please try a new one.";
$lang["user_already_exist_try_new_one"]="User already exists. Please try a new one.";

$lang["special_char_not_allowed_fname"]='Special characters are not allowed on First Name.';
$lang["number_not_allowed_fname"]='Numbers are not allowed on First Name.';

$lang["special_char_not_allowed_mname"]='Special characters are not allowed on Middle Name.';
$lang["number_not_allowed_mname"]='Numbers are not allowed on Middle Name.';
 
 $lang["special_char_not_allowed_lname"]='Special characters are not allowed on Last Name.';
$lang["number_not_allowed_lname"]='Numbers are not allowed on Last Name.';
 
 $lang["skype_id_already_exist_try_new_one"]="Skype ID already exists. Please try a new one.";
 
 $lang["invitation_code_does_not_exist"]="Invitation Code is invalid.";
 $lang["invalid_email_or_username"]="Invalid Username/Email or Password.";
 $lang["email_not_verified"]="Your account is not yet verified.";
 
 $lang["email_not_exists"]="Email is not registered in your account.";
 $lang["email_alrady_verified"]="Email already verified.";
$lang["user_does_not_exists"]="User does not exists.";
 
 $lang["invalid_old_pass"]="Old password is invalid.";
 $lang["invalid_referral_code"]="Referral Code is invalid.";
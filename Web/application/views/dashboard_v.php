<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Xantau Dashboard</title>

    <!-- Bootstrap -->
    <link href="<?php echo base_url()?>vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?php echo base_url()?>vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="<?php echo base_url()?>vendors/iCheck/skins/flat/green.css" rel="stylesheet">
    <!-- bootstrap-progressbar -->
    <link href="<?php echo base_url()?>vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
    <!-- jVectorMap -->
    <link href="<?php echo base_url()?>assets/css/maps/jquery-jvectormap-2.0.3.css" rel="stylesheet" />

    <!-- Custom Theme Style -->
    <link href="<?php echo base_url()?>assets/css/custom.min.css" rel="stylesheet">
</head>

<body class="nav-md">
    <div class="container body">
        <div class="main_container">
            <div class="col-md-3 left_col">
                <div class="left_col scroll-view">
                    <div class="navbar nav_title" style="border: 0;">
                        <a href="index.html" class="site_title"><i class="fa fa-tint"></i> <span>Xantau</span></a>
                    </div>

                    <div class="clearfix"></div>



                    <br />

                    <!-- sidebar menu -->
                    <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                        <div class="menu_section">
                            <h3>General</h3>
                            <ul class="nav side-menu">
                                <li><a><i class="fa fa-home"></i> Dashboard</a>
                                </li>
                            </ul>
                        </div>


                    </div>
                    <!-- /sidebar menu -->

                    <!-- /menu footer buttons -->
                    <div class="sidebar-footer hidden-small">
                        <a data-toggle="tooltip" data-placement="top" title="Settings">
                            <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
                        </a>
                        <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                            <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
                        </a>
                        <a data-toggle="tooltip" data-placement="top" title="Lock">
                            <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
                        </a>
                        <a data-toggle="tooltip" data-placement="top" title="Logout">
                            <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
                        </a>
                    </div>
                    <!-- /menu footer buttons -->
                </div>
            </div>

            <!-- top navigation -->
            <div class="top_nav">
                <div class="nav_menu">
                    <nav class="" role="navigation">
                        <div class="nav toggle">
                            <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                        </div>


                    </nav>
                </div>
            </div>
            <!-- /top navigation -->

            <!-- page content -->
            <div class="right_col" role="main">
                <!-- top tiles -->
                <div class="row tile_count">
                    <div class="col-md-3 col-sm-3 col-xs-6 tile_stats_count">
                        <span class="count_top"><i class="fa fa-user"></i> Savings</span>
                        <div class="count" id="savingswaste"><i class="green"><i class="fa fa-sort-asc"></i>100%</i>
                        </div>
                        <span class="count_bottom">As of today</span>
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-6 tile_stats_count">
                        <span class="count_top"><i class="fa fa-user"></i> Daily</span>
                        <div class="count" id="dailysavingswaste"><i class="green"><i class="fa fa-sort-asc"></i>100%</i>
                        </div>
                        <span class="count_bottom">As of today</span>
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-6 tile_stats_count">
                        <span class="count_top"><i class="fa fa-user"></i> Monthly</span>
                        <div class="count" id="monthlysavingswaste"><i class="red"><i class="fa fa-sort-desc"></i>38%</i>
                        </div>
                        <span class="count_bottom">As of today</span>
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-6 tile_stats_count">
                        <span class="count_top"><i class="fa fa-user"></i> Peso Value</span>
                        <div class="count"><i class="green">₱999</i>
                        </div>
                        <span class="count_bottom">As of today</span>
                    </div>
                </div>
                <!-- /top tiles -->

                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <div class="x_content">
                                <div id="mainb" style="height:350px;"></div>
                            </div>
                        </div>
                    </div>

                </div>

                <br />
            </div>
            <!-- /page content -->

            <!-- footer content -->
            <footer>
                <div class="pull-right">
                    Xantau 2016
                </div>
                <div class="clearfix"></div>
            </footer>
            <!-- /footer content -->
        </div>
    </div>

    <!-- jQuery -->
    <script src="<?php echo base_url()?>vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?php echo base_url()?>vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo base_url()?>vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="<?php echo base_url()?>vendors/nprogress/nprogress.js"></script>
    <!-- Chart.js -->
    <script src="<?php echo base_url()?>vendors/Chart.js/dist/Chart.min.js"></script>
    <!-- gauge.js -->
    <script src="<?php echo base_url()?>vendors/bernii/gauge.js/dist/gauge.min.js"></script>
    <!-- bootstrap-progressbar -->
    <script src="<?php echo base_url()?>vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
    <!-- iCheck -->
    <script src="<?php echo base_url()?>vendors/iCheck/icheck.min.js"></script>
    <!-- Skycons -->
    <script src="<?php echo base_url()?>vendors/skycons/skycons.js"></script>
    <!-- Flot -->
    <script src="<?php echo base_url()?>vendors/Flot/jquery.flot.js"></script>
    <script src="<?php echo base_url()?>vendors/Flot/jquery.flot.pie.js"></script>
    <script src="<?php echo base_url()?>vendors/Flot/jquery.flot.time.js"></script>
    <script src="<?php echo base_url()?>vendors/Flot/jquery.flot.stack.js"></script>
    <script src="<?php echo base_url()?>vendors/Flot/jquery.flot.resize.js"></script>
    <!-- Flot plugins -->
    <script src="<?php echo base_url()?>assets/js/flot/jquery.flot.orderBars.js"></script>
    <script src="<?php echo base_url()?>assets/js/flot/date.js"></script>
    <script src="<?php echo base_url()?>assets/js/flot/jquery.flot.spline.js"></script>
    <script src="<?php echo base_url()?>assets/js/flot/curvedLines.js"></script>
    <!-- jVectorMap -->
    <script src="<?php echo base_url()?>assets/js/maps/jquery-jvectormap-2.0.3.min.js"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="<?php echo base_url()?>assets/js/moment/moment.min.js"></script>
    <script src="<?php echo base_url()?>assets/js/datepicker/daterangepicker.js"></script>
    
        <!-- ECharts -->
    <script src="<?php echo base_url()?>vendors/echarts/dist/echarts.min.js"></script>
    <script src="<?php echo base_url()?>vendors/echarts/map/js/world.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="<?php echo base_url()?>assets/js/custom.min.js"></script>



    <!-- jVectorMap -->
    <script src="<?php echo base_url()?>assets/js/maps/jquery-jvectormap-world-mill-en.js"></script>
    <script src="<?php echo base_url()?>assets/js/maps/jquery-jvectormap-us-aea-en.js"></script>
    <script src="<?php echo base_url()?>assets/js/maps/gdp-data.js"></script>
    <script src="<?php echo base_url()?>assets/js/ajaxfunctions.js"></script>
    <script src="<?php echo base_url()?>assets/js/customxantau.js"></script>
</body>

</html>
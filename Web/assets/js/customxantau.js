var highTreshold = 4;
var OPTIMALMINUTES = 120;
var READINGINTERVAL = 10;

var OPTIMALCOUNTLIMIT = OPTIMALMINUTES / READINGINTERVAL;
var percentagePerReading = 100 / OPTIMALCOUNTLIMIT;

var totalSavingsWaste = 0;
var totalDailySavingsWaste = 0;

var lastInsertedId = 0;
var currentSessionData = [];



var theme = {
    color: [
        '#BDC3C7', '#408829', '#ff3232', '#3498DB',
              '#9B59B6', '#8abb6f', '#759c6a', '#bfd3b7'
          ],

    title: {
        itemGap: 8,
        textStyle: {
            fontWeight: 'normal',
            color: '#408829'
        }
    },

    dataRange: {
        color: ['#1f610a', '#97b58d']
    },

    toolbox: {
        color: ['#408829', '#408829', '#408829', '#408829']
    },

    tooltip: {
        backgroundColor: 'rgba(0,0,0,0.5)',
        axisPointer: {
            type: 'line',
            lineStyle: {
                color: '#408829',
                type: 'dashed'
            },
            crossStyle: {
                color: '#408829'
            },
            shadowStyle: {
                color: 'rgba(200,200,200,0.3)'
            }
        }
    },

    textStyle: {
        fontFamily: 'Arial, Verdana, sans-serif'
    }
};

$(document).ready(function () {
    getAllData();
        setInterval(getAllData, 10000);

});


function getAllData() {
    callAjaxRequestJSON(filterData, responseError);
}

function filterData(response) {


    responseSuccess(response);
}

function responseSuccess(response) {
    var dataPoints = [];
    var dataTime = [];
    var colors = [];
    var lastDateDataString;
    if (response.feeds.length > 0) {
        var isWasting = false;
        var feedList = response.feeds;
        for (index = 0; index < feedList.length; ++index) {
            var f = feedList[index];
            var createdAt = f.created_at;
            var entryId = f.entry_id;
            var fieldConsumed = f.field1;

            dataPoints.push(fieldConsumed);


            var readDate = new Date(createdAt);

            var hours = readDate.getHours();
            var minutes = readDate.getMinutes();

            if (index >= (feedList.length - 1)) {
                lastDateDataString = getDateString(createdAt);
            }

            dataTime.push(hours + ":" + minutes);

            if (entryId > lastInsertedId) {

                currentSessionData.push(f);
                lastInsertedId = entryId;
                computeSavingsWaste();
                updateSavingsWaste();
                updateDailySavingsWaste();
            }

            console.log(hours + ":" + minutes + " -> " + totalSavingsWaste + " ->" + totalDailySavingsWaste);

            if (totalSavingsWaste < 0) {
                isWasting = true;
            } else {
                isWasting = false;
            }

            if (fieldConsumed >= highTreshold) {


                if (isWasting) {
                    colors.push("#ff1919");
                } else {
                    colors.push("#C6E579");
                }
            } else {
                colors.push("#27727B");
            }



        }



    }
    setChartData(dataPoints, dataTime, colors, lastDateDataString);
}

function responseError(response) {
    alert("Fail");
}

function getDateString(dateString) {
    var dateString = new Date(dateString);

    var monthNames = [
      "January", "February", "March",
      "April", "May", "June", "July",
      "August", "September", "October",
      "November", "December"
    ];
    var day = dateString.getDate();
    var monthIndex = dateString.getMonth();
    var year = dateString.getFullYear();

    var hours = dateString.getHours();
    var minutes = dateString.getMinutes();
    var seconds = dateString.getSeconds();

    var returnDateString = monthNames[monthIndex] + " " + day + ", " + year + " " + hours + ":" + minutes + ":" + seconds;
    return returnDateString;
}


function setChartData(dataset, datatime, colors, lastReadDate) {

    var echartBar = echarts.init(document.getElementById('mainb'));

    echartBar.setOption({
        title: {
            text: 'Sensor Activities',
            subtext: 'Last Recorded data : ' + lastReadDate
        },
        tooltip: {
            trigger: 'axis'
        },
        toolbox: {
            show: false
        },
        calculable: false,
        xAxis: [{
            type: 'category',
            data: datatime
        }],
        yAxis: [{
            type: 'value'
        }],
        series: [{
            name: 'reading',
            type: 'bar',
            itemStyle: {
                normal: {
                    color: function (params) {
                        // build a color map as your need.
                        var colorList = colors;
                        return colorList[params.dataIndex]
                    }
                }
            },
            data: dataset
        }]
    });
}

function computeSavingsWaste() {
    var isStartingToCount = false;
    totalSavingsWaste = 100;
    totalDailySavingsWaste = 0;
    for (index = 0; index < currentSessionData.length; ++index) {
        var d = currentSessionData[index];
        var createdAt = d.created_at;
        var entryId = d.entry_id;
        var fieldConsumed = d.field1;



        if (!isStartingToCount && fieldConsumed < highTreshold) {

            totalSavingsWaste = 100;
            isStartingToCount = true;


        }
        if (isStartingToCount) {
            if (fieldConsumed >= highTreshold) {
                totalSavingsWaste -= percentagePerReading;

            } else {
                totalDailySavingsWaste = (totalDailySavingsWaste+totalSavingsWaste)/2;
                totalSavingsWaste = 100;
            }
        }

    }

}

function updateSavingsWaste() {

    if (totalSavingsWaste >= 0) {
        setSavingsValue(totalSavingsWaste);
    } else {
        setWasteValue(totalSavingsWaste);
    }

}

function setSavingsValue(value) {
    $("#savingswaste").html('<i class="green"><i class="fa fa-sort-asc"></i>' + value.toFixed(2) + '%</i>');
}

function setWasteValue(value) {
    $("#savingswaste").html('<i class="red"><i class="fa fa-sort-desc"></i>' + value.toFixed(2) + '%</i>');
}


function updateDailySavingsWaste() {

    if (totalDailySavingsWaste >= 0) {
        setDailySavingsValue(totalDailySavingsWaste);
    } else {
        setDailyWasteValue(totalDailySavingsWaste);
    }

}

function setDailySavingsValue(value) {
    $("#dailysavingswaste").html('<i class="green"><i class="fa fa-sort-asc"></i>' + value.toFixed(2) + '%</i>');
    $("#monthlysavingswaste").html('<i class="green"><i class="fa fa-sort-asc"></i>' + value.toFixed(2) + '%</i>');
}

function setDailyWasteValue(value) {
    $("#dailysavingswaste").html('<i class="red"><i class="fa fa-sort-desc"></i>' + value.toFixed(2) + '%</i>');
    $("#monthlysavingswaste").html('<i class="red"><i class="fa fa-sort-desc"></i>' + value.toFixed(2) + '%</i>');
}